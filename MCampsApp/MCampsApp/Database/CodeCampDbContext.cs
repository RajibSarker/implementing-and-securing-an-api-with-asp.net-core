﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MyCodeCamp.Data.Entities;

namespace MCampsApp.Database
{
    public class CodeCampDbContext:DbContext
    {
        public CodeCampDbContext(DbContextOptions options):base(options)
        {

        }
        public DbSet<Camp> Camps { get; set; }
        public DbSet<Speaker> Speakers { get; set; }
        public DbSet<Talk> Talks { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
        public static CodeCampDbContext Create(IConfiguration configuration)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CodeCampDbContext>();
            optionsBuilder.UseNpgsql(configuration.GetConnectionString("AppConnectionString"));
            var context = new CodeCampDbContext(optionsBuilder.Options);
            return context;
        }
    }
}
