﻿using MCampsApp.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MyCodeCamp.Data;

namespace MCampsApp.Configurations
{
    public class ServiceConfiguration
    {
        public static void Configuration(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<CodeCampDbContext>(c => c.UseNpgsql(configuration.GetConnectionString("AppConnectionString")));

            // apply seeds
            services.AddTransient<CampDbInitializer>();

            // service resolve
            services.AddTransient<ICampRepository, CampRepository>();
        }
    }
}
