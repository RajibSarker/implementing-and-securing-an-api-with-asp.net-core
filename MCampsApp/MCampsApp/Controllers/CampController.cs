﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyCodeCamp.Data;
using MyCodeCamp.Data.Entities;
using System;
using System.Threading.Tasks;

namespace MCampsApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CampController : ControllerBase
    {
        private readonly ICampRepository _campRepository;
        private readonly ILogger<CampController> logger;

        public CampController(ICampRepository campRepository, ILogger<CampController> logger)
        {
            _campRepository = campRepository;
            this.logger = logger;
        }
        public IActionResult Get()
        {
            return Ok(_campRepository.GetAllCamps());
        }

        [HttpGet("{id}", Name ="GetCamp")]
        public IActionResult Get(int id, bool includeSpeakers = false)
        {
            try
            {
                Camp camp = null;
                if (includeSpeakers) camp = _campRepository.GetCampWithSpeakers(id);

                else camp = _campRepository.GetCamp(id);
                return Ok(camp);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Camp model)
        {
            try
            {
                logger.LogInformation("Creating new camp to database.");
                _campRepository.Add(model);
                if (await _campRepository.SaveAllAsync())
                {
                    var returnUri = Url.Link("GetCamp", new { id = model.Id });
                    return Created(returnUri, model);
                }
                else
                {
                    logger.LogWarning("Camp data could not be saved to database. Please try again later. Thank you.");
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Exception occured while creating camp data. The exception is {ex}");
            }
            return BadRequest();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Camp model)
        {
            try
            {
                var oldCamp = _campRepository.GetCamp(id);
                if (oldCamp == null) return NotFound($"There is no camp found for id {id}");

                // map the object
                oldCamp.Name = model.Name ?? oldCamp.Name;
                oldCamp.Description = model.Description ?? oldCamp.Description;
                oldCamp.Length = model.Length>0?model.Length:oldCamp.Length;   
                oldCamp.Location = model.Location??oldCamp.Location;
                oldCamp.EventDate = model.EventDate != DateTime.MinValue ? model.EventDate : oldCamp.EventDate;

                if (await _campRepository.SaveAllAsync())
                {
                    return Ok(oldCamp);
                }
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return BadRequest($"Camp update failed for id {id}");
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var oldCamp = _campRepository.GetCamp(id);
                if (oldCamp == null) return NotFound($"No camp found with ID {id}");

                _campRepository.Delete(oldCamp);
                if(await _campRepository.SaveAllAsync())
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return BadRequest("Could not delete the camp.");
        }
        
    }
}
