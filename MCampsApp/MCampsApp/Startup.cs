﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MCampsApp.Configurations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MyCodeCamp.Data;
using Newtonsoft.Json;

namespace MCampsApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(1);
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddDistributedMemoryCache();



            services.AddControllers().AddNewtonsoftJson(options =>
            {
                //options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
            services.Configure<FormOptions>(x =>
            {
                //x.MultipartBodyLengthLimit = 209715200;
                x.MultipartBodyLengthLimit = long.MaxValue;
            });
            services.AddCors(policy =>
            {
                policy.AddPolicy("AllowAll", option =>
                {
                    option.AllowAnyHeader();
                    option.AllowAnyMethod();
                    option.AllowAnyOrigin();
                });
            });

            ServiceConfiguration.Configuration(services, Configuration);


            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, CampDbInitializer seeder)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseRouting();
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoint => {
                endpoint.MapControllers();
                //endpoint.MapHub<MessageHub>("/message", (options) =>
                //{
                //    options.Transports = HttpTransportType.WebSockets;
                //    //new HubConfiguration { EnableJSONP = true };
                //});
                //endpoint.MapHangfireDashboard("/hangfire", new DashboardOptions
                //{
                //    Authorization = new[] { new HangfireAuthorizationFilter() }
                //});
            });

            // apply sees
            seeder.Seed().Wait();
        }
    }
}
